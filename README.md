# README #

### JDP Internship website ###

### How do I get set up? ###

1. Install Node

* On a Mac: `brew install node`
* (if you don't have brew installed: http://brew.sh/)

* On Windows: https://nodejs.org/en/download/package-manager/#windows

2. Run `npm install`

3. Run `npm start`.

4. Read `gulpfile.js` to understand how the pipeline works.

### License

All content is licensed CC-BY-SA-4.0 (convertible to GNU GPL 3)*, except where covered by another license.

* see: http://www.gnu.org/licenses/license-list.en.html#ccby

#### Contributing

Accepting pull requests, as long as they are contributed with a compatible license.
