$(function () {
  // Handler for .ready() called.
  youtubeApiCall(); // Initial call
  $("#pageTokenNext").on("click", function (event) { //click More videos
    youtubeApiCall();
  });
});

function youtubeApiCall() {
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "https://sdfddeq5k5.execute-api.us-east-1.amazonaws.com/dev/jdpvideoAll",
    "method": "POST",
    "headers": {
      "content-type": "application/json"
    },
    "processData": false,
    "data": "{\r\n  \"pageToken\": \"" + $("#pageToken").val() + "\"\r\n}"
  };

  $.ajax(settings).done(function (data) {
    if (typeof data.nextPageToken === "undefined") {
      $("#pageTokenNext").hide();
    } else {
      $("#pageTokenNext").show();
    }
    var items = data.items, videoList = "";
    $("#pageToken").val(data.nextPageToken);

    $.each(items, function (index, e) {
      var aRef = e.snippet.title;
      //Template for video listing
      videoList = videoList +
                    '<div class="Video" href="//www.youtube.com/watch?v=' + e.id.videoId + '" data-lity>\n\
                      <div class="imagePreview">\n\
                        <img alt="' + e.snippet.title + '" src="' + e.snippet.thumbnails.default.url + '">\n\
                      </div>\n\
                      <div class="content">\n\
                        <h4>' + e.snippet.title + '</h4>\n\
                        <p>' + e.snippet.description + '</p>\n\
                      </div>\n\
                    </div>';

    });
    $(".VideoFeed").append($(videoList).hide().fadeIn('slow')); //animate append of videos
  });
}