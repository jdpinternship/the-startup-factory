'use strict';

/**
 *
 * This gulpfile starts a server and refreshes the browser whenever a change is saved
 * It also watches the file system and compiles sass and jade to HTML5 and CSS when they are saved
 * This is a pretty standard mdern web development pipeline
 * This conforms to the agile ideology as changes are "continuously deployed" to the browser when a file is saved
 *
 * It also encapsulates the bulk of the wiring and setup
 * Node.js is a favoured platform for this, since web developers use Javascript
 * 
 * This gulpfile uses:
 * 	Browsersync: https://www.browsersync.io/docs/gulp/
 *  gulp-sass: https://www.npmjs.com/package/gulp-sass
 *  gulp-jade: https://www.npmjs.com/package/gulp-jade
 *
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var jade = require('gulp-jade');
var browserSync = require('browser-sync').create();

// Static server using browser-sync
gulp.task('browser-sync', function() {
	
	browserSync.init({
		
		server: {
			
			baseDir: "./build"
			
		}
		
	});
	
	// gulp.watch tasks the filesystem and trigger the compile tasks
	// this watches the sass files and compiles them
	gulp.watch('./src/*.scss', ['sass:watch']);
	gulp.watch('./src**/*.scss', ['sass:watch']);
	
	// this watches the jade files and compiles them
	gulp.watch('./src/*.jade', ['jade:watch']);
	gulp.watch('./src/**/*.jade', ['jade:watch']);
	
	// this watches all other files in the src folder ??
	gulp.watch('./src/**/*', ['assets']);
	gulp.watch('./src/**', ['assets']);
	
});

/**
 *
 * We decorate the sass and jade compile tasks to have then complete (as dependencies) before
 * reloading the browser
 *
 * See: https://en.wikipedia.org/wiki/Decorator_pattern
 * And: https://medium.com/@dave_lunny/task-dependencies-in-gulp-b885c1ab48f0#.g306xoxea
 *
 */

gulp.task('sass:watch', ['sass'], browserSync.reload);
gulp.task('jade:watch', ['jade'], browserSync.reload);

/**
 *
 * Gulp sass tasks compiles sass files in /src/scss and pipes them to /build
 * See: https://sitepoint.com/simple-gulpy-workflow-sass/
 *
 */
gulp.task('sass', function() {

	return gulp.src('./src/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./build/'));

});

/**
 *
 * Gulp jade task compiles jade templates in /src and pipes then to /build
 * 
 */
gulp.task('jade', function() {

	var YOUR_LOCALS = {};
	
	gulp.src('./src/*.jade')
		.pipe(jade({
			locals: YOUR_LOCALS
		}))
		.pipe(gulp.dest('./build/'));
	
	gulp.src('./src/bio/*.jade')
		.pipe(jade({
			locals: YOUR_LOCALS
		}))
		.pipe(gulp.dest('./build/bio'));

	gulp.src('./src/projects/*.jade')
		.pipe(jade({
			locals: YOUR_LOCALS
		}))
		.pipe(gulp.dest('./build/projects'));

});

/**
 *
 * Gulp assets task - probably the most simple gulp task that can be made
 * It streams /src/assets into /build, effectively copying the directory
 *
 * You could set up a file watcher for the directory if you want
 * this is left as an exercise for the reader
 *
 */

gulp.task('assets', function() {
	
	gulp.src('./src/assets/**/*')
		.pipe(gulp.dest('./build/assets/'));
	
	gulp.src('./src/views/**/*')
		.pipe(gulp.dest('./build/views'));
	
	gulp.src('./src/bio/**/*')
		.pipe(gulp.dest('./build/bio'));

});

/**
 *
 * This is the main gulp task, in the package.json file, you'll
 * see that it is called by the start script, so 'npm start' will call this task
 * 
 */

gulp.task('dev', ['assets','sass','jade', 'browser-sync']);

/**
 *
 * this set of tasks builds the site.
 *
 */

// this task builds the assets as long as the previous assets task has been completed
gulp.task('build:assets', ['assets']);

// this task builds the sass as long as the previous assets and sass tasks has been completed
gulp.task('build:sass', ['build:assets', 'sass']);

// this task builds the jade as long as the previous sass and jade tasks has been completed
gulp.task('build:jade', ['build:sass', 'jade']);

// this task builds the build as long as the previous jade task has been completed
gulp.task('build', ['build:jade'], function() {
	console.log('Done');
});
